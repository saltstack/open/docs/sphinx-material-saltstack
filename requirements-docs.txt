sphinx>=3.3.1
# sphinx-material-saltstack==1.0.5
sphinx-tabs>=3.2.0
sphinx-copybutton>=0.3.1
sphinxcontrib-spelling>=7.1.0
Sphinx-Substitution-Extensions>=2020.9.30.0

# If needed custom 404 page, when docs
## hosted on custom domain like readthedocs.io
sphinx-notfound-page>=0.5
