black==20.8b1
pre-commit==2.9.3
isort==5.6.4
twine==3.2.0
towncrier==19.2.0