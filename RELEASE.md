# Release Process

This is for releasing new versions of the theme to pypi!

## Preqrequisites

User must have permissions to the pypi repository:
- https://pypi.org/project/sphinx-material-saltstack/

## Building the theme

```bash
# Pull the latest updates from upstream theme
git submodule update --init --recursive

# This takes the submodule, creates a folder called sphinx_material_saltstack,
## and overwrites files with what is found in saltstack-customizations
./tools/pre-setup.sh
```

## Publish to PyPI

When preparing to generate the tar files, a few pieces need to be taken care of first:
- `umask 022`

`umask`, though, needs to be properly defined _before cloning_ a repo in the first place. So, before publishing, you may need to make a fresh clone before following the next steps.

```bash
# Building for pypi
python3 setup.py sdist
twine check dist/*
```

```bash
# Publishing to pypi
twine upload dist/*
```